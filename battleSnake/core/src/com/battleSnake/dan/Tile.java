package com.battleSnake.dan;

import Server.ColorParser;
import Server.GameClient;
import com.badlogic.gdx.graphics.Color;
import java.awt.Rectangle;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.battleSnake.kess.Drawable;
/**
 *
 * @author Dan
 */
public class Tile extends Drawable {
    public static int SIZE = 8;
    protected Rectangle bounds = new Rectangle();
    private final int getX;
    private final int getY;

    
    /**
     * Creates the tile
     * @param xV x position of tile
     * @param yV y position of tile
     * @param depth
     */
    public Tile(int xV, int yV, int depth){
        super(depth);
	bounds = new Rectangle(xV,yV,SIZE,SIZE);
        getX = xV;
        getY = yV;

    }
    
    /**
     * draws the tile on the screen
     * @param batch batch to draw the tiles
     */
    @Override
    public void draw(SpriteBatch batch) {
        batch.setColor(Color.RED);
        batch.draw(GameClient.MASTER_SPRITE, bounds.x, bounds.y, SIZE, SIZE);
    }
    
    public int getX(){
        return getX;
    }
      
    public int getY(){
        return getY;
    }    

    @Override
    public String getDraw() {
        return " " + key + "," + bounds.x + "," + bounds.y + "," + ColorParser.storeColor(Color.BLACK);
    }
}
