package com.battleSnake.dan;

import Server.GameClient;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import static com.battleSnake.dan.Tile.SIZE;
import com.battleSnake.kess.Collidable;
import java.awt.Rectangle;
import java.io.Serializable;

/**
 *
 * @author Dan
 */
public class Wall extends Tile implements Collidable, Serializable{    
    public Wall(int x, int y, int depth) {
        super(x, y, depth);
    }

    @Override
    public Rectangle getBounds() {
        return bounds;
    }
    
    @Override
    public void draw(SpriteBatch b) {
        b.setColor(Color.BLACK);
        b.draw(GameClient.MASTER_SPRITE, bounds.x, bounds.y, SIZE, SIZE);
    }
}
