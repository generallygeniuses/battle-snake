package com.battleSnake;

import Server.GameClient;
import Server.GameServer;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.battleSnake.dan.Tile;
import com.battleSnake.kess.MapReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BattleSnake extends ApplicationAdapter {
    private static int WIDTH = Tile.SIZE * 100, HEIGHT = Tile.SIZE * 50;
    SpriteBatch batch;
    GameClient game;
    GameServer gServer;
    Socket[] players = null;
    ServerSocket server;
    Socket player;
    int numPlay, ai;

    public BattleSnake(Socket[] toClients, int numAI) {
        players = toClients;
        numPlay = toClients.length + 1;
        ai = numAI;
    }

    public BattleSnake(String ip) {
        try {
            player = new Socket(ip, 9999);
        } catch (IOException ex) {
            Logger.getLogger(BattleSnake.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void create () {
        GameClient.MASTER_SPRITE = 
                new Texture(Gdx.files.internal("snake.png"));
        Gdx.graphics.setDisplayMode(WIDTH, HEIGHT, false);

        //File file = new File(Gdx.files.internal("level.snk").path());
        //System.out.println(file.getPath());
        //AssetManager assets = new AssetManager();

        batch = new SpriteBatch();
        int w = Gdx.graphics.getWidth()/Tile.SIZE;
        int h = Gdx.graphics.getHeight()/Tile.SIZE;

        //if host start server and client
        if(players != null) {
            gServer = new GameServer(players, ai, MapReader.makeMap(w, h));
        } else {
            game = new GameClient(player, MapReader.makeMap(w, h));
        }
    }

    @Override
    public void render () {            
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();

        if(gServer != null) {
            gServer.draw(batch);
        } else {
            game.draw(batch);
        }

        batch.end();

        if(gServer != null) {
            if(!gServer.getRun())
                dispose();
        } else if (!game.getRun()){
            dispose();
        }
    }

    @Override
    public void dispose() {
        if(gServer != null)
            gServer.dispose();
        else
            game.dispose();
        super.dispose();
    }
}
