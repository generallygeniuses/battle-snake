/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.battleSnake.kess;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import java.io.Serializable;

/**
 *
 * @author kdunc004
 */
public abstract class Drawable implements Comparable, Serializable {
    private static int ID = 0;
    protected int depth;
    public final int key = ID++;
    
    public Drawable(int d) {
        depth = d;
    }
    public abstract void draw(SpriteBatch batch);
    
    @Override
    public int compareTo(Object c) {
        if(c instanceof Drawable) {
            Drawable other = (Drawable) c;
            if(other.depth > this.depth) return 1;
            else if(other.depth < this.depth) return -1;
            else return 0;
        } else return 0;
    }
    
    public abstract String getDraw();
}
