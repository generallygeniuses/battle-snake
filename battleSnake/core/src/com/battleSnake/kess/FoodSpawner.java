package com.battleSnake.kess;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.battleSnake.alex.SpawnPoint;
import com.battleSnake.dan.Tile;
import com.battleSnake.dan.Wall;
import com.battleSnake.kess.Power.Invincible;
import com.battleSnake.kess.Power.PowerUp;
import com.battleSnake.kess.RunArgs.AIArgs;
import com.battleSnake.kyle.Food;
import java.util.LinkedList;
import java.util.Random;

/**
 *
 * @author Kess
 */
public class FoodSpawner extends Drawable implements Runnable {
    private static final int NUM = 4;
    long elapsedTime = 0, powerUpTimer = 15000;
    private PowerUp[] onBoard = new PowerUp[NUM];
    private Food[] food = new Food[NUM];
    private Invincible power;
    private Random rand = new Random(System.currentTimeMillis());
    
    public FoodSpawner(int d) {
        super(d);
        power = new Invincible(0, 0, 0);
        for(int i = 0; i < NUM; i++) {
             onBoard[i] = food[i] = new Food(0);
             food[i].die();
        }
    }
    
    @Override
    public void run(RunArgs args) {
        AIArgs data = (AIArgs) args;
        elapsedTime += data.deltaTime;
        for(int i = 0; i < NUM; i++) {
            if(!onBoard[i].isActive()) {
                onBoard[i] = spawnFood(i, data.map);
            }
            onBoard[i].run(data.getTimeArgs());
        }
        power.run(data.getTimeArgs());
        if(power.hasSnake()) {
            elapsedTime = 0;
        }
    }
    
    public PowerUp[] getOnBoard() {
        return onBoard;
        }

    @Override
    public boolean isActive() {
        return true;
    }
    
    /**
     * Spawns food on the map.
     * @param index the food to spawn on the map.
     */
    private PowerUp spawnFood(int index, Tile[][] arena) {
        Tile tmp;
        int x, y;
        do {
            x = rand.nextInt(arena.length);
            y = rand.nextInt(arena[0].length);

            tmp = arena[x][y];
        } while(tmp instanceof Wall || tmp instanceof SpawnPoint);
        
        if(elapsedTime > powerUpTimer && !power.isActive() && !power.hasSnake()) {
            power.spawn(tmp.getX(), tmp.getY());
            elapsedTime = 0;
            return power;
        } else {
            food[index].spawn(tmp.getX(), tmp.getY());
            return food[index];
        }
    }
    
    public LinkedList<Collidable> getCollidables() {
        LinkedList<Collidable> value = new LinkedList<Collidable>();
        for(int i = 0; i < NUM; i++) {
            value.add(onBoard[i]);
        }
        return value;
    }

    @Override
    public void draw(SpriteBatch batch) {
        for(int i = 0; i < NUM; i++) {
            onBoard[i].draw(batch);
        }
        if(power.isActive())
            power.draw(batch);
    }

    @Override
    public String getDraw() {
        return "";
    }
    
    public LinkedList<Drawable> getDrawables() {
        LinkedList<Drawable> value = new LinkedList<Drawable>();
        for(int i = 0; i < NUM; i++) {
            value.add(onBoard[i]);
        }
        if(power.isActive())
                value.add(power);
        return value;
    }
}
