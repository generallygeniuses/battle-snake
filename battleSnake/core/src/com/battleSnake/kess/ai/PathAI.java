package com.battleSnake.kess.ai;

import com.battleSnake.alex.MasterSnake;
import static com.battleSnake.alex.SnakeNode.Direction.*;
import com.battleSnake.kess.Power.PowerUp;
import com.battleSnake.kess.RunArgs;
import com.battleSnake.kess.RunArgs.AIArgs;
import com.battleSnake.kess.ai.Path.PathNode;
import com.battleSnake.kyle.Food;
import java.awt.Point;
import java.util.Random;

/**
 *
 * @author kdunc004
 */
public class PathAI extends AI {
    private int destination = -1;
    private PathNode path = null;
    
    public PathAI(MasterSnake s) {
        super(s);
    }
    
    @Override
    public void run(RunArgs args) {
        AIArgs tmp = (AIArgs) args;
        if(destination == -1)
            destination = getRandomFood(tmp.food);
        
        Point end = new Point(tmp.food[destination].getBounds().x, 
                tmp.food[destination].getBounds().y);
        Point start = new Point(snake.getHeadX(), snake.getHeadY());
        
        path = (new Path(start, end, tmp.map)).getPath();
        if(path != null) {           
            snake.clearQueue();
            int deltaX = path.getLocation().x - start.x;
            int deltaY = path.getLocation().y - start.y;

            if(deltaX < 0) { //the snake is to the right of the next move
                if(snake.getDirection().equals(EAST)) 
                    snake.setDirection(NORTH);
                else snake.setDirection(WEST);
            } else if (deltaX > 0) { //the snake is to the left of the next move
               if(snake.getDirection().equals(WEST)) 
                    snake.setDirection(SOUTH);
                else snake.setDirection(EAST);
            } else if (deltaY < 0) { //the snake is above the next move
                if(snake.getDirection().equals(NORTH)) 
                    snake.setDirection(WEST);
                else snake.setDirection(SOUTH);
            } else if (deltaY > 0) { //the snake is below the next move
                if(snake.getDirection().equals(SOUTH)) 
                    snake.setDirection(EAST);
                else snake.setDirection(NORTH);
            }
            path = path.getParent();
            if(path == null)
                destination = -1;
        }
        snake.run(tmp.getTimeArgs());
    }
    
    private int getRandomFood(PowerUp[] foods) {
        return RAND.nextInt(foods.length);
    }
    
    private int getClosestFood(Food[] foods) {
        int value = 0;
        double distance = Double.MAX_VALUE;
        for(int i = 0; i < foods.length; i++) {
            Food tmpFood = foods[i];
            
            //get distance
            int deltaX = tmpFood.getBounds().x - snake.getHeadX();
            int deltaY = tmpFood.getBounds().y - snake.getHeadY();
            double tmpDistance = Math.sqrt(
                Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
            
            if(tmpDistance < distance) {
                distance = tmpDistance;
                value = i;
            }
        }
        return value;
    }    
}
