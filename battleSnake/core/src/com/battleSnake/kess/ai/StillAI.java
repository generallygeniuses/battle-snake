package com.battleSnake.kess.ai;

import com.battleSnake.alex.MasterSnake;
import com.battleSnake.kess.RunArgs;
import com.battleSnake.kess.RunArgs.AIArgs;

/**
 * An AI that makes the snake sit still and occasionally grow.
 * @author Kess
 */
public class StillAI extends AI {
    private final long growthRate = 20;
    private long elapsedTime = 0;
    
    public StillAI (MasterSnake s) {
        super(s);
    }
    
    @Override
    public void run(RunArgs a) {
        AIArgs args = (AIArgs) a;
        elapsedTime += args.deltaTime;
        if (elapsedTime > growthRate) {
            snake.addSnakeNode();
            elapsedTime = 0;
        }
        
        snake.run(args.getTimeArgs());
    }
}
