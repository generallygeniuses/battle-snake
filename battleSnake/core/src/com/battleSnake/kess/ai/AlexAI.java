package com.battleSnake.kess.ai;

import com.battleSnake.alex.MasterSnake;
import static com.battleSnake.alex.SnakeNode.Direction.EAST;
import static com.battleSnake.alex.SnakeNode.Direction.NORTH;
import static com.battleSnake.alex.SnakeNode.Direction.SOUTH;
import static com.battleSnake.alex.SnakeNode.Direction.WEST;
import com.battleSnake.dan.Tile;
import com.battleSnake.kess.Collidable;
import com.battleSnake.kess.Power.PowerUp;
import com.battleSnake.kess.RunArgs;
import java.awt.Rectangle;

/**
 *
 */
public class AlexAI extends AI {
    private PowerUp cFood;
    private PowerUp[] fArray;
    private double tempDistance, distance;
    private boolean avoidCollide, makeMove = true;
    private int xCompass;
    private int yCompass;
        
    public AlexAI(MasterSnake s) {
        super(s);
    }  
        
    public void closestFood(){
        distance = Integer.MAX_VALUE;
        makeMove = true;
        for (PowerUp fArray1 : fArray) {
            
            //Distance is sqrt( deltaX^2 + deltaY^2 )
            int deltaX = fArray1.getBounds().x - snake.getHeadX();
            int deltaY = fArray1.getBounds().y - snake.getHeadY();
            tempDistance = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
           
            if(tempDistance <= Tile.SIZE) {
                //snake.clearQueue();
            } else if (tempDistance < distance) {
                distance = tempDistance;
                cFood = fArray1;
            }
        }
        xCompass = cFood.getBounds().x - snake.getHeadX();
        yCompass = cFood.getBounds().y - snake.getHeadY();  
        
        if(xCompass == 0 || yCompass == 0)
            snake.clearQueue();
    }
    
    @Override
    public void run(RunArgs a) {
        //Extract agruments
        RunArgs.AIArgs temp = (RunArgs.AIArgs) a;
        
        //update food.
        fArray = temp.food;
        closestFood();
        avoidCollide = true;
        

        //TODO predict collision with wall. This can be done by comparing the bounds
        //of a Sight object to the tiles in the map that fall within that sight.
        
        //TODO predict collision with other snakes. Use the MasterSnake.collides method
        //and pass in a sight object.
        
        //We do not need to attempt to predict collision with food as we want to hit it.

        //Make a move.
        switch(snake.getDirection()) {
            case NORTH: 
                if (avoidCollide == true){
                    if (xCompass > 0) snake.setDirection(EAST);
                    else snake.setDirection(WEST);
                } else if (avoidCollide == false){
                    if (yCompass <= 0){
                        if (xCompass >0) snake.setDirection(EAST);
                        else snake.setDirection(WEST);
                    }
                }
                break;
            case EAST: 
                if (avoidCollide == true){
                    if (yCompass >0) snake.setDirection(NORTH);
                    else snake.setDirection(SOUTH);
                }else if (avoidCollide == false){
                    if (xCompass <= 0){
                        if (yCompass >0) snake.setDirection(NORTH);
                        else snake.setDirection(SOUTH);
                    }
                }
                break;
            case SOUTH: 
                if (avoidCollide == true){
                    if (xCompass >0) snake.setDirection(EAST);
                    else snake.setDirection(WEST);
                }else if (avoidCollide == false){
                    if (yCompass >= 0){
                        if (xCompass >0) snake.setDirection(EAST);
                        else snake.setDirection(WEST);
                    }
                }
                break;
            case WEST: 
                if (avoidCollide == true){
                    if (yCompass >0) snake.setDirection(NORTH);
                    else snake.setDirection(SOUTH);
                }else if (avoidCollide == false){
                    if (xCompass >= 0){
                        if (yCompass >0) snake.setDirection(NORTH);
                        else snake.setDirection(SOUTH);
                    }
                }
                break;
            default: break;
        }
        
        //Run the snake
        snake.run(temp.getTimeArgs());
        
        //Update values
    }
}
