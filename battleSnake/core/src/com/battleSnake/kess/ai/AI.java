package com.battleSnake.kess.ai;

import com.battleSnake.alex.MasterSnake;
import com.battleSnake.dan.Tile;
import com.battleSnake.kess.Collidable;
import com.battleSnake.kess.Runnable;
import java.awt.Rectangle;
import java.util.Random;

/**
 *
 * @author Kess
 */
public abstract class AI implements Runnable {
    protected MasterSnake snake;
    protected static Random RAND = new Random(System.currentTimeMillis());
    
    public AI(MasterSnake s) {
        snake = s;
    }
    
    public MasterSnake getSnake() {
        return snake;
    }
    
    @Override
    public boolean isActive() {
        return snake.isActive();
    }
    
    public class Sight implements Collidable {
        private boolean isColliding = false;
        private int x, y;
        
        public void setColliding(boolean c) {
            isColliding = c;
        }
        
        public boolean getColliding() {
            return isColliding;
        }
        
        //Move setColBounds method to here since it is possible to 
        //generate the rectangle dynamically rather than store it in memory.
        @Override 
        public Rectangle getBounds() {
            switch(snake.getDirection()) {
                case NORTH: 
                    x = snake.getHeadX(); 
                    y = snake.getHeadY() + Tile.SIZE; 
                    break;
                case EAST: 
                    x = snake.getHeadX() + Tile.SIZE; 
                    y = snake.getHeadY(); 
                    break;
                case SOUTH: 
                    x = snake.getHeadX(); 
                    y = snake.getHeadY() - Tile.SIZE; 
                    break;
                case WEST: 
                    x = snake.getHeadX() - Tile.SIZE; 
                    y = snake.getHeadY(); 
                    break;
                default: break;
            }
            return new Rectangle(x, y, Tile.SIZE, Tile.SIZE);
        }
    }
}
