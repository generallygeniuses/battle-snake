package com.battleSnake.kess.ai;

import com.battleSnake.dan.Tile;
import com.battleSnake.dan.Wall;
import java.awt.Point;
import java.util.LinkedList;
import java.util.PriorityQueue;

/**
 *
 * @author Kess
 */
public class Path {
    LinkedList<PathNode> nodes;
    PathNode start;
    PathNode path;
    Point destination;

    public Path(Point dest, Point start, Tile[][] map) {
        //we will start from our destination and build our path backwards.
        this.start = new PathNode(dest, dest, null);
        destination = start;
        path = makePath(map);
    }
    
    public PathNode getPath() {
        return path;
    }

    /**
     * Uses a* to form a path to follow
     * @param map the arena
     */
    private PathNode makePath(Tile[][] map) {
        PriorityQueue<PathNode> open = new PriorityQueue<PathNode>();
        PathNode[][] closed = new PathNode[map.length][map[0].length];
        open.add(start);

        while(open.size() > 0) {
            PathNode current = open.poll();
            closed[current.getXIndex()][current.getYIndex()] = current;
            
            if(current.location.equals(destination)) {
                return current;
            }
            
            //add all of currents neighbors to the list if they are not a wall.
            int x, y;
            for(int i = 0; i < 4; i++) {
                PathNode tmp;
                switch(i) {
                    case 0: //NORTH
                        x = current.location.x;
                        y = current.location.y + Tile.SIZE;
                        break;
                    case 1: //EAST
                        x = current.location.x + Tile.SIZE;
                        y = current.location.y;
                        break;
                    case 2: //WEST
                        x = current.location.x - Tile.SIZE;
                        y = current.location.y;
                        break;
                    case 3: //SOUTH
                        x = current.location.x;
                        y = current.location.y - Tile.SIZE;
                        break;
                    default: x = y = 0;
                }
                tmp = new PathNode(new Point(x,y), destination, current);
                if(tmp.inBounds(map) && !(tmp.getTile(map) instanceof Wall)) {
                    PathNode test = closed[tmp.getXIndex()][tmp.getYIndex()];
                    if(test == null) {
                        open.add(tmp);
                    } else {
                        if(test.testParent(current)) {
                            test.setParent(current);
                        }
                    }
                }
            }
        }
        
        return null;
    }

    public class PathNode implements Comparable {
        Point location, destination;
        PathNode parent;
        int score;
        
        public PathNode (Point location, Point destination, PathNode p) {
            this.location = location;
            this.destination = destination;
            parent = p;
            makeScore(destination);
        }
        
        public PathNode getParent() {
            return parent;
        }
        
        public Point getLocation() {
            return location;
        }
        
        // returns true if the provided parent is a better option
        public boolean testParent(PathNode p) {
            PathNode test = new PathNode(location, destination, p);
            return test.compareTo(this) < 0;
        }
        
        public Tile getTile(Tile[][] map) {
            return map[getXIndex()][getYIndex()];
        }
        
        public boolean inBounds(Tile[][] map) {
            return getXIndex() >= 0 && getXIndex() < map.length
                    && getYIndex() >= 0 && getYIndex() < map[0].length;
        }
        
        public void setParent(PathNode p) {
            parent = p;
        }
        
        public int getXIndex() {
            return location.x/Tile.SIZE;
        }
        
        public int getYIndex() {
            return location.y/Tile.SIZE;
        }
        
        private void makeScore(Point dest) {
//            if(parent == null) {
//                score = 0;
//            } else {
//                score += parent.score;
//            }
            Point tmp = new Point(location.x, location.y);
            
            //Caluclate a distance heuristic.
            while(tmp.x != dest.x) {
                if(tmp.x < dest.x) {
                    tmp.x += Tile.SIZE;
                } else if(tmp.x > dest.x) {
                    tmp.x -= Tile.SIZE;
                }
                score += 10;
            }
            
            while(tmp.y != dest.y) {
                if(tmp.y < dest.y) {
                    tmp.y += Tile.SIZE;
                } else if(tmp.y > dest.y) {
                    tmp.y -= Tile.SIZE;
                }
                score += 10;
            }
        }

        @Override
        public boolean equals(Object o) {
            if(o instanceof PathNode) {
                PathNode other = (PathNode) o;
                return location.equals(other.location);
            } else return false;
        }
        
        @Override
        public int compareTo(Object o) {
            if(o instanceof PathNode) {
                PathNode other = (PathNode) o;
                if(this.score < other.score) return -1;
                else if(this.score > other.score) return 1;
                else return 0;
            } else return 0;
        }
    }
}
