package com.battleSnake.kess.Power;

import com.battleSnake.kess.Collidable;
import com.battleSnake.kess.Drawable;
import com.battleSnake.kess.RunArgs;
import com.battleSnake.kess.RunArgs.TimeArgs;
import com.battleSnake.kess.Runnable;

/**
 *
 * @author Kess
 */
public abstract class PowerUp extends Drawable implements Runnable, Collidable {
    protected long elapsedTime = 0;

    public PowerUp(int d) {
        super(d);
    }
    
    @Override
    public void run(RunArgs args) {
        TimeArgs time = (TimeArgs) args;
        elapsedTime += time.deltaTime;
    }
    
    /**
     * moves the power up to the given location
     * @param x
     * @param y 
     */
    public abstract void spawn(int x, int y);
}
