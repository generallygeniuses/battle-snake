package com.battleSnake.kess.Power;

import Server.GameClient;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.battleSnake.alex.MasterSnake;
import com.battleSnake.dan.Tile;
import com.battleSnake.kess.Collidable;
import com.battleSnake.kess.RunArgs;
import java.awt.Rectangle;

/**
 *
 * @author Kess
 */
public class Invincible extends PowerUp implements Collidable {
    private Rectangle bounds;
    private MasterSnake snake = null;
    private final long duration = 10000;
    private int colorChange, changeRate = 100;
    private boolean active = false;
    int color = 0, prevColor = -3;;
    
    public Invincible(int x, int y, int d) {
        super(d);
        bounds = new Rectangle(x,y,Tile.SIZE,Tile.SIZE);
    }
    
    @Override
    public void run(RunArgs args) {
        super.run(args);
        if(snake == null) {
            elapsedTime = 0;
            color++;
        } else {
            if(elapsedTime > duration && snake != null) {
                snake.setInvincible(false);
                snake.setColor(prevColor);
                snake = null;
            } else {
                if(elapsedTime > colorChange) {
                    snake.setColor(color++%(MasterSnake.COLORS.length/2));
                    colorChange += changeRate;
                }
            }
        }
    }

    @Override
    public void draw(SpriteBatch batch) {
        if(snake == null) {
            batch.setColor(MasterSnake.COLORS[color%(MasterSnake.COLORS.length/2)]);
            batch.draw(GameClient.MASTER_SPRITE, bounds.x, bounds.y, Tile.SIZE, Tile.SIZE);
        }
    }

    @Override
    public String getDraw() {
        if(snake == null) return "" + key + "," + bounds.x + "," + bounds.y + "," + 6;
        else return "" + key + "," + bounds.x + "," + bounds.y + "," + -2;
    }

    @Override
    public boolean isActive() {
        return active;
    }
    
    public void collides(MasterSnake s) {
        if(snake == null) {
            snake = s;
            snake.setInvincible(true);
            elapsedTime = 0;
            active = false;
            prevColor = snake.getColor();
        }
    }    

    @Override
    public Rectangle getBounds() {
        return bounds;
    }    

    @Override
    public void spawn(int x, int y) {
        elapsedTime = 0;
        active = true;
        bounds.setLocation(x, y);
        colorChange = changeRate;
    }
    
    public boolean hasSnake() {
        return snake != null;
    }
}
