package com.battleSnake.kess.Power;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.battleSnake.alex.MasterSnake;
import com.battleSnake.kess.RunArgs;
import java.awt.Rectangle;

/**
 *
 * @author Kess
 */
public class SpeedBoost extends PowerUp {
    private short duration = 2000, coolDown = 2000, animation = 2000;
    private MasterSnake boost;
    private int color;
    private boolean frameControl = false;
    
    public SpeedBoost(MasterSnake boost) {
        super(0);
        this.boost = boost;
        color = boost.getColor();
    }
    
    @Override
    public void run(RunArgs args) {
        super.run(args);
        //Run the snake a second time to effectivley double its speed.
        if(elapsedTime < duration)
            boost.run(args);
        else if(elapsedTime < duration + coolDown) {
            //boost ready animation
        }
        else if(elapsedTime < duration + coolDown + animation) {
            //return boost to regular color
        }
    }
    
    @Override
    public boolean isActive() {
        return elapsedTime <= duration + coolDown + animation;
    }    

    @Override
    public void draw(SpriteBatch batch) {
        //do nothing
    }

    @Override
    public String getDraw() {
        return "" + key + "," + 0 + "," + 0 + "," + -1;
    }

    @Override
    public void spawn(int x, int y) {
    }

    //NOT USED
    @Override
    public Rectangle getBounds() {
        return null;
    }
    
    @Override
    public boolean equals(Object o) {
        if(o instanceof SpeedBoost) {
            SpeedBoost other = (SpeedBoost) o;
            return boost.equals(other.boost);
        } return false;
    }
}
