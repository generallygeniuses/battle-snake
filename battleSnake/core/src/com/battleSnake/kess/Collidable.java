package com.battleSnake.kess;

import java.awt.Rectangle;

/**
 * A collidable is just an object that has bounds that can be collided with.
 * @author kdunc004
 */
public interface Collidable {
    public Rectangle getBounds();
}
