package com.battleSnake.kess;

import Server.GameServer;
import com.badlogic.gdx.Input.Keys;
import com.battleSnake.alex.MasterSnake;
import com.battleSnake.alex.SnakeNode;
import com.battleSnake.kess.Power.SpeedBoost;

/**
 *
 * @author kdunc004
 */
public class SnakeController implements com.badlogic.gdx.InputProcessor {
    private final MasterSnake snake;
    private final GameServer server;
    
    public SnakeController (MasterSnake s, GameServer server) {
        snake = s;
        this.server = server;
    }

    @Override
    public boolean keyDown(int i) {
        switch(i) {
            case Keys.W: 
                if(snake.getDirection() == SnakeNode.Direction.EAST ||
                    snake.getDirection() == SnakeNode.Direction.WEST)
                        snake.setDirection(SnakeNode.Direction.NORTH); 
                break;
            case Keys.A:  
                if(snake.getDirection() == SnakeNode.Direction.NORTH ||
                    snake.getDirection() == SnakeNode.Direction.SOUTH)
                        snake.setDirection(SnakeNode.Direction.WEST); 
                break;
            case Keys.S:  
                if(snake.getDirection() == SnakeNode.Direction.EAST ||
                    snake.getDirection() == SnakeNode.Direction.WEST)
                        snake.setDirection(SnakeNode.Direction.SOUTH); 
                break;
            case Keys.D:  
                if(snake.getDirection() == SnakeNode.Direction.NORTH ||
                    snake.getDirection() == SnakeNode.Direction.SOUTH)
                        snake.setDirection(SnakeNode.Direction.EAST); 
                break;
            case Keys.SPACE:
                server.addBoost(new SpeedBoost(snake));
                break;
                
        }
        
        return true;
    }

    @Override
    public boolean keyUp(int i) {
        return false;
    }

    @Override
    public boolean keyTyped(char c) {
        return false;
    }

    @Override
    public boolean touchDown(int i, int i1, int i2, int i3) {
        return false;
    }

    @Override
    public boolean touchUp(int i, int i1, int i2, int i3) {
        return false;
    }

    @Override
    public boolean touchDragged(int i, int i1, int i2) {
        return false;
    }

    @Override
    public boolean mouseMoved(int i, int i1) {
        return false;
    }

    @Override
    public boolean scrolled(int i) {
        return false;
    }
    
}
