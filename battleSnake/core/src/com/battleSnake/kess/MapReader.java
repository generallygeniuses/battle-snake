package com.battleSnake.kess;

import com.battleSnake.alex.SnakeNode;
import com.battleSnake.alex.SnakeNode.Direction;
import com.battleSnake.alex.SpawnPoint;
import com.battleSnake.dan.Tile;
import com.battleSnake.dan.Wall;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * MapReader is a static helper class that takes in a .snk
 * file and reads it into an arena.
 * @author kdunc004
 */
public class MapReader {
    
    public static Tile[][] readMap(File file) {
        Tile[][] tiles = null;
        
        try {
            Scanner reader = new Scanner(file);
            String line;
            int lineNum = -1;
            
            while(reader.hasNextLine()) {
                line = reader.nextLine();
                if(tiles == null) {
                    Scanner parser = new Scanner(line);
                    parser.useDelimiter(",");
                    tiles = new Tile[parser.nextInt()][parser.nextInt()];
                } else {
                    for(int i = 0; i < line.length(); i++) {
                        char next = line.charAt(i);
                        switch(next) {
                            case '#': tiles[lineNum][i] = 
                                    new Wall(lineNum * Tile.SIZE, i * Tile.SIZE, 10);
                                break;
                            case '.':
                                if(lineNum == 1) {
                                    tiles[lineNum][i] = 
                                        new SpawnPoint(lineNum * Tile.SIZE, i * Tile.SIZE, 10, Direction.EAST);
                                } else if(lineNum == tiles.length - 2) {
                                    tiles[lineNum][i] = 
                                        new SpawnPoint(lineNum * Tile.SIZE, i * Tile.SIZE, 10, Direction.WEST);
                                } else if(i == 1) {
                                    tiles[lineNum][i] = 
                                        new SpawnPoint(lineNum * Tile.SIZE, i * Tile.SIZE, 10, Direction.NORTH);
                                } else if(i == tiles[lineNum].length - 2) {
                                    tiles[lineNum][i] = 
                                        new SpawnPoint(lineNum * Tile.SIZE, i * Tile.SIZE, 10, Direction.SOUTH);
                                } else {
                                    tiles[lineNum][i] = 
                                        new Tile(lineNum * Tile.SIZE, i * Tile.SIZE, 0);
                                }
                                break;
                        }
                    }
                }
                lineNum++;
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MapReader.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("OUCH!");
        }
        
        return tiles;
    }
    
    public static Tile[][] makeMap(int w, int h) {
        Tile[][] tmp = new Tile[w][h];
        for(int i = 0; i < w; i++)
            for(int j = 0; j < h; j++){
                if(i == 0 || j == 0 || i == w-1 || j == h-1)
                    tmp[i][j] = new Wall(i * Tile.SIZE, j * Tile.SIZE, 0);
                else if(i == 1) {
                    tmp[i][j] = 
                        new SpawnPoint(i * Tile.SIZE, j * Tile.SIZE, 10, 
                                SnakeNode.Direction.EAST);
                } else if(i == tmp.length - 2) {
                    tmp[i][j] = 
                        new SpawnPoint(i * Tile.SIZE, j * Tile.SIZE, 10, 
                                SnakeNode.Direction.WEST);
                } else if(j == 1) {
                    tmp[i][j] = 
                        new SpawnPoint(i * Tile.SIZE, j * Tile.SIZE, 10, 
                                SnakeNode.Direction.NORTH);
                } else if(j == tmp[i].length - 2) {
                    tmp[i][j] = 
                        new SpawnPoint(i * Tile.SIZE, j * Tile.SIZE, 10, 
                                SnakeNode.Direction.SOUTH);
                } else {
                    tmp[i][j] = 
                        new Tile(i * Tile.SIZE, j * Tile.SIZE, 0);
                }
            }
        return tmp;
    }
}
