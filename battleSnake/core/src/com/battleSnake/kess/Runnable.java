package com.battleSnake.kess;

/**
 * A runnable is an object that can be run and can be active or inactive.
 * @author kdunc004
 */
public interface Runnable {
    public void run(RunArgs args);
    public boolean isActive();
}
