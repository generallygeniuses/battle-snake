package com.battleSnake.kess;

import com.battleSnake.alex.MasterSnake;
import com.battleSnake.dan.Tile;
import com.battleSnake.kess.Power.PowerUp;
import com.battleSnake.kess.ai.AI;
import com.battleSnake.kyle.Food;

/**
 *
 * @author kdunc004
 */
public abstract class RunArgs {
    
    public static RunArgs makeArgs(Runnable r, Tile[][] map, PowerUp[] food, MasterSnake[]snakes, long deltaTime) {
        if(r instanceof AI || r instanceof FoodSpawner) {
            return new AIArgs(deltaTime, map, snakes, food);
        } else {
            return new TimeArgs(deltaTime);
        }
    }
    
    public static class AIArgs extends RunArgs {
        public final Tile[][] map;
        public final MasterSnake[] snakes;
        public final long deltaTime;
        public final PowerUp[] food;

        public AIArgs(long d, Tile[][] m, MasterSnake[] s, PowerUp[] f) {
            snakes = s;
            map = m;
            deltaTime = d;
            food = f;
        }
        
        public TimeArgs getTimeArgs() {
            return new TimeArgs(deltaTime);
        }
    }
    
    public static class TimeArgs extends RunArgs {
        public final long deltaTime;
        
        public TimeArgs(long d) {
            deltaTime = d;
        }
    }
}
