package com.battleSnake.kess;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.battleSnake.dan.Tile;
import com.battleSnake.kess.RunArgs.TimeArgs;

/**
 *
 * @author Kess
 */
public class GameTimer extends Drawable implements Runnable {
    public static BitmapFont font = new BitmapFont();
    private int elapsedTime = 0, duration = 180000; // 3min 180000
    private int x, y;
    private String timer = "";
    private boolean useString = false;

    public GameTimer(Tile[][] map, int d) {
        super(d);
        font.setColor(Color.BLACK);
        x = ((map.length*Tile.SIZE)/2);
        y = ((map[0].length*Tile.SIZE)/2) - (int)(font.getLineHeight()/2);
    }
    
    @Override
    public void draw(SpriteBatch batch) {        
        if(!useString)
            font.draw(batch, getTimeString(), x, y);
        else
            font.draw(batch, timer, x, y);
    }

    @Override
    public String getDraw() {
        return "TIME," + getTimeString();
    }

    @Override
    public void run(RunArgs args) {
        TimeArgs time = (TimeArgs) args;
        elapsedTime += time.deltaTime;
    }

    @Override
    public boolean isActive() {
        return elapsedTime <= duration;
    }
    
    public String getTimeString() {
        int tmp = duration - elapsedTime;
        int min = tmp/60000;
        int tens = (tmp/10000)%6;
        int sec = (tmp/1000)%10;
        return "" + min + ":" + tens + "" + sec;
    }
    
    public void set(String timer) {
        useString = true;
        this.timer = timer;
    }
}
