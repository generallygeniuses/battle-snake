package com.battleSnake.kyle;

import Server.GameClient;
import com.badlogic.gdx.graphics.Color;
import java.awt.Rectangle;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.battleSnake.alex.MasterSnake;
import com.battleSnake.kess.Collidable;
import com.battleSnake.dan.Tile;
import com.battleSnake.kess.Power.PowerUp;
import com.battleSnake.kess.RunArgs;
import com.battleSnake.kess.RunArgs.TimeArgs;
import com.battleSnake.kess.Runnable;
import java.io.Serializable;
import java.util.Random;

/**
 * @author Kyle Williams
 * @date Sept 23, 2015
 */
public class Food extends PowerUp implements Collidable, Runnable, Serializable {
    private Rectangle location;
    private boolean alive = false;
    private int color = 0;
    
    /**
     * Creates the food object
     * @param depth depth to draw the food at
     */
    public Food(int depth) {        
        super(depth);
        //color = Color.BLUE;
        location = new Rectangle(0,0, Tile.SIZE, Tile.SIZE);
        alive = true;
        color = MasterSnake.COLORS.length/2 - 1;
    }
    
    /**
     * Draws the food object
     * @param spr
     */
    @Override
    public void draw(SpriteBatch spr){
        if(alive){
            Color prev = spr.getColor().cpy();
            spr.setColor(MasterSnake.COLORS[color]);
            spr.draw(GameClient.MASTER_SPRITE,
                    (int)location.getX(),
                    (int)location.getY(),
                    (int)Tile.SIZE,
                    (int)Tile.SIZE);
            spr.setColor(prev);            
        }
    }
    
    /**
     * Destroys the food object
     */
    public void die(){
        setActive(false);
    }

    /**
     * Moves the food to the given location
     * @param x 
     * @param y 
     */
    public void setLocation(int x, int y) {
        this.location = new Rectangle(x, y, Tile.SIZE, Tile.SIZE);
    }   

    /**
     * @param alive the alive to set
     */
    public void setActive(boolean alive) {
        this.alive = alive;
    }

    /**
     * @return the location and size of the food in the game world.
     */
    @Override
    public Rectangle getBounds() {
        return location;
    }

     /**
     * @return whether or not the food is active
     */
    @Override
    public boolean isActive() {
        return alive;
    }
    
    /**
     * Runs the food animation
     * @param a arguments to run the foods animation
     */
    @Override
    public void run(RunArgs a) {
    }

    /**
     * sets the food to active and moves it to the given location
     * @param x
     * @param y 
     */
    @Override
    public void spawn(int x, int y) {
        alive = true;
        setLocation(x, y);
    }

    @Override
    public String getDraw() {
        if(isActive())
            return "" + key + "," + location.x + "," + location.y + "," 
                + color;
        else
            return "" + key + "," + location.x + "," + location.y + "," 
                + -1;
    }
}