package com.battleSnake.alex;

import com.battleSnake.alex.SnakeNode.Direction;
import com.battleSnake.kess.Collidable;
import com.battleSnake.dan.Tile;
import com.battleSnake.kess.RunArgs;
import com.battleSnake.kess.Runnable;
import java.awt.Rectangle;

/**
 * @author atorr006
 */

public class SpawnPoint extends Tile implements Collidable, Runnable {
    private final Rectangle spawnBounds, spawn;

    public boolean isAvailable = true;
    private MasterSnake collidingSnake;
    private Direction direction;
    private int cSnakeSize;
    private int runCounter;
    
    public SpawnPoint(int x, int y, int depth, Direction dir){
        super(x, y, depth);
        direction = dir;
        
        switch(dir) {
            case NORTH: spawn = new Rectangle(x, y + Tile.SIZE, Tile.SIZE, Tile.SIZE); break;
            case SOUTH: spawn = new Rectangle(x, y, Tile.SIZE, Tile.SIZE); break;
            case EAST: spawn = new Rectangle(x + Tile.SIZE, y, Tile.SIZE, Tile.SIZE); break;
            case WEST: spawn = new Rectangle(x, y, Tile.SIZE, Tile.SIZE); break;
            default: spawn = new Rectangle(x, y, Tile.SIZE, Tile.SIZE); break;
        }
        
        spawnBounds = new Rectangle(x - Tile.SIZE, y - Tile.SIZE, 3 * Tile.SIZE, 3 * Tile.SIZE);
    }

    @Override
    public Rectangle getBounds() {
        return spawnBounds;
    }
    
    public Rectangle getSpawn() {
        return spawn;
    }

    public void isNotAvailable(){
        isAvailable = false;
    }

    public void isAvailable(){
        isAvailable = true;
    }

    public boolean getAvailability(){
        return isAvailable;
    }

    public void snakeCollides(MasterSnake snakeInput){
        collidingSnake = snakeInput;
        cSnakeSize = collidingSnake.snakeSize();
        isAvailable = false;
        runCounter = 0;
    }

    @Override
    public void run(RunArgs args) {
        if(collidingSnake != null) {
            cSnakeSize = collidingSnake.snakeSize();
            runCounter++;
            if (runCounter == cSnakeSize){
                isAvailable = true;
            }
        }
    }    

    @Override
    public boolean isActive() {
        return true;
    }

    public Direction getDirection() {
        return direction;
    }
}

