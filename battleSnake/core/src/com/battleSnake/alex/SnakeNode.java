package com.battleSnake.alex;

import Server.ColorParser;
import Server.GameClient;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.battleSnake.dan.Tile;
import com.battleSnake.kess.Drawable;
import java.awt.Rectangle;

/**
 *
 * @author atorr006
 */
public class SnakeNode extends Drawable {
    private SnakeNode nextNode;
    private final boolean hardBody;
    private boolean active = true;
    private int color = 0;
    
    // The nodes direction and location in the game world
    private Direction dir;
    private Rectangle bounds;
    
    public SnakeNode(int x, int y, Direction d, boolean bool, int c, int depth) {
        super(depth);
        hardBody = bool;
        nextNode = null;
        dir = d;
        bounds = new Rectangle(x, y, Tile.SIZE, Tile.SIZE);
        color = c;
    }
    
    /**
     * @return whether or not the specified node is hard
     */
    public boolean isHard() {
        return hardBody;
    }
    
    /**
     * @return the node trailing this node
     */
    public SnakeNode getNext(){
        return nextNode;
    }
    
    /**
     * Sets the child of this node to the provided node
     * @param setNextNode the child of this node
     */
    public void setNext(SnakeNode setNextNode){
        if(setNextNode == null) setNextNode.getDraw(); // crash and follow the stack trace
        nextNode = setNextNode;
    }
    
    /**
     * Returns the rectangular bounds of the snake node.
     * This includes the x,y location.
     * @return the space and area the node takes up in the game world. 
     */
    public Rectangle getBounds() {
        return bounds;
    }
    
    /**
     * @return the direction the node is moving in
     */
    public Direction getDirection() {
        return dir;
    }

    @Override
    public void draw(SpriteBatch batch) {
        Color prev = batch.getColor().cpy();
        if(!hardBody) {
            batch.setColor(MasterSnake.COLORS[color + (MasterSnake.COLORS.length/2)]);
        }
        if(active)batch.draw(GameClient.MASTER_SPRITE, bounds.x, bounds.y, Tile.SIZE, Tile.SIZE);
        batch.setColor(prev);
    }
    
    /**
     * Moves the node x and y tiles in the give x and y directions
     * @param x left or right
     * @param y up or down
     */
    public void move(int x, int y) {
        if(nextNode != null)
            nextNode.setLocation(bounds.x, bounds.y);
        bounds.translate(x * Tile.SIZE, y * Tile.SIZE);
    }
    
    /**
     * Sets the location of the node to the given x and y
     * @param x
     * @param y 
     */
    private void setLocation(int x, int y) {
        if(nextNode != null)
            nextNode.setLocation(bounds.x, bounds.y);
        bounds.setLocation(x,y);
    }

    /**
     * Sets the node to move in a given direction
     * @param direction 
     */
    void setDirection(Direction direction) {
        dir = direction;
    }

    @Override
    public String getDraw() {
        return "" + key + "," + bounds.x + "," + bounds.y;
    }
    
    /**
     * Direction is used to both move the snake node in the proper direction
     * as well as add its next node in the proper space behind it.
     */
    public enum Direction {
        NORTH, SOUTH, EAST, WEST;
    }
    
    public void setActive(boolean val) {
        active = val;
        if(nextNode != null && !val)
            nextNode.setActive(false);
    }
    
    public boolean isActive() {
        return active;
    }
    
    public void setColor(int c) {
        color = c;
        
        SnakeNode tmp = getNext();
        if(tmp != null && tmp.active)
            tmp.setColor(c);
    }
}
