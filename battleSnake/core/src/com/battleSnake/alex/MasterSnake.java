package com.battleSnake.alex;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.battleSnake.alex.SnakeNode.Direction;
import com.battleSnake.dan.Tile;
import com.battleSnake.dan.Wall;
import com.battleSnake.kess.Collidable;
import com.battleSnake.kess.Drawable;
import com.battleSnake.kess.Power.Invincible;
import com.battleSnake.kess.RunArgs;
import com.battleSnake.kess.RunArgs.TimeArgs;
import com.battleSnake.kess.Runnable;
import com.battleSnake.kess.ai.AlexAI;
import com.battleSnake.kyle.Food;
import java.io.Serializable;
import java.util.LinkedList;

/**
 * @author atorr006
 */
public class MasterSnake extends Drawable implements Runnable, Serializable {
    private static final boolean[] NODE_SPAWN_ORDER = {
        true, true, true, true, true, true, false, false, false
    };
    static float[] toAdd = {.4f, 1f};

    public static final Color[] COLORS = {
        Color.RED, Color.BLUE, Color.FOREST, Color.BROWN, Color.PURPLE, Color.BLACK, Color.GOLD,
        Color.RED.cpy().add(toAdd[0], toAdd[0], toAdd[0], toAdd[1]),
        Color.BLUE.cpy().add(toAdd[0], toAdd[0], toAdd[0], toAdd[1]), 
        Color.FOREST.cpy().add(toAdd[0], toAdd[0], toAdd[0], toAdd[1]), 
        Color.BROWN.cpy().add(toAdd[0], toAdd[0], toAdd[0], toAdd[1]), 
        Color.PURPLE.cpy().add(toAdd[0], toAdd[0], toAdd[0], toAdd[1]),
        Color.BLACK.cpy().add(toAdd[0], toAdd[0], toAdd[0], toAdd[1]), 
        Color.GOLD.cpy().add(toAdd[0], toAdd[0], toAdd[0], toAdd[1]), 
    };

    private int nodeCounter, spawnSize = 5, newLength = 0, deathPenalty = 3, 
            elapsedTime, growthRate = 10000, cutRation = 3;
    private SnakeNode head;
    private SnakeNode lastNode;
    private int color;
    private boolean active = true;
    private Direction current, queued;
    private boolean powered = false;

    /**
     * Creates a new Master Snake.
     * @param point the spawn point to spawn at
     * @param c  the player number of the snake
     * @param depth the depth the snake is drawn at
     */
    public MasterSnake(SpawnPoint point, int c, int depth) {
        super(depth);
        color = c;
        head = new SnakeNode(0, 0, point.getDirection(), true, color, depth);
        respawn(point);        
    }

    /**
     * Adds a new snake node at the tail of the snake
     */
    public void addSnakeNode() {
        if(lastNode.getNext() != null) {
            lastNode.getNext().setActive(true);
            lastNode = lastNode.getNext();
            nodeCounter++;
        } else {
            lastNode.setNext(makeNode(lastNode, 
                    NODE_SPAWN_ORDER[nodeCounter%NODE_SPAWN_ORDER.length]));
            lastNode = lastNode.getNext();
            nodeCounter++;
        }
    }

    /**
     * @return the size of the snake
     */
    public int snakeSize(){
        return nodeCounter;
    }

    public void setColor(int color) {
        this.color = color;
        head.setColor(color);
    }

    /**
     * Sets the snake to inactive
     */     
    public void die() { 
        active = false;
        if(!powered)
            newLength = nodeCounter - deathPenalty;
        else
            newLength = nodeCounter;
    }

    /**
     * Creates a new SnakeNode based on the position and direction of its parent.
     * @param parent the parent to the snake node
     * @param hard whether or not the new node should be hard
     * @return the new node
     */
    private SnakeNode makeNode(SnakeNode parent, boolean hard) {
        // new x value and new y value
        int nX = 0,nY = 0;
        // The node will be placed based on the parents direction
        switch(parent.getDirection()) {
            case NORTH: // place it to the south of the parent
                nX = parent.getBounds().x; // get the x location of head.next
                nY = parent.getBounds().y - Tile.SIZE; //the y location of head.next
                break;
            case EAST: // place it to the west of the parent
                nX = parent.getBounds().x - Tile.SIZE; // get the x location of head.next
                nY = parent.getBounds().y; //the y location of head.next
                break;
            case SOUTH: // place it the the north of the parent
                nX = parent.getBounds().x; // get the x location of head.next
                nY = parent.getBounds().y + Tile.SIZE; //the y location of head.next
                break;
            case WEST: // place it to the east of the parent
                nX = parent.getBounds().x + Tile.SIZE; // get the x location of head.next
                nY = parent.getBounds().y; //the y location of head.next
                break;
            default: break;
        }
        return new SnakeNode(nX, nY, parent.getDirection(), hard, color, depth);
    }

    /**
     * Draws the snake node by node
     * @param batch the batch to draw with.
     */
    @Override
    public void draw(SpriteBatch batch) {
        // Set the color to the snakes color
        batch.setColor(COLORS[color]);
        SnakeNode current = head;
        while(current != null) {
            current.draw(batch);
            current = current.getNext();
        }
    }

    /**
     * Cuts the snake at the given index
     * @param index the index to cut at
     */
    public void cut(int index) {
        lastNode = head;
        while(index-- != 1) {
            nodeCounter--;
            lastNode = lastNode.getNext();
        }
        lastNode.setActive(false);
        nodeCounter--;
    }

    /**
     * Test and resolves any collisions against another snake
     * @param other the snake to test against
     */
    public void collides(MasterSnake other) {
        if(!other.equals(this) && 
                this.head.getBounds().intersects(other.head.getBounds())) {
            if(this.powered && other.powered)
            {
                this.die();
                other.die();
            } else {
                if(!this.powered)this.die();
                if(!other.powered)other.die();
            }
        }

        int count = 0, add = 0;
        SnakeNode prev = other.head;
        SnakeNode tmp = other.head.getNext();
        while(tmp != null && tmp.isActive()) {
            if(this.head.getBounds().intersects(tmp.getBounds())) {
                if((other.powered || tmp.isHard()) && !this.powered) {
                    this.die();
                    return;
                } else if(this.powered || (!tmp.isHard() && !other.powered)) {
                    tmp.setActive(false);
                    other.lastNode = prev;
                    add = (other.nodeCounter - count)/cutRation;
                    other.nodeCounter = ++count;
                    
                    for(int i = 0; i < add; i++)
                        this.addSnakeNode();
                    return;
                }
            }
            prev = tmp;
            tmp = tmp.getNext();
            count++;
        }
    }

    /**
     * Tests and resolves collisions with static collidable objects
     * @param other the object to test against
     */
    public void collides(Collidable other) {
        if(this.head.getBounds().intersects(other.getBounds())) {
            if(other instanceof Food) {
                Food tmp = (Food) other;
                tmp.die();
                this.addSnakeNode();
            } else if(other instanceof Wall) {
                this.die();
            } else if (other instanceof SpawnPoint){
                SpawnPoint tmp = (SpawnPoint) other;
                tmp.snakeCollides(this);
            } else if(other instanceof AlexAI.Sight) {
                ((AlexAI.Sight)other).setColliding(true);
            } else if(other instanceof Invincible) {
                Invincible p = (Invincible) other;
                p.collides(this);
            }
        }
    }

    /**
     * Moves the snake in its direction
     * @param a the arguments needed to run the snake
     */
    @Override
    public void run(RunArgs a) {
        TimeArgs args = (TimeArgs) a;
        elapsedTime += args.deltaTime;
        if(elapsedTime > growthRate) {
            addSnakeNode();
            spawnSize++;
            elapsedTime = 0;
        }
        
        if(current != null)
            head.setDirection(current);

        switch(head.getDirection()) {
            case NORTH: head.move(0,1);
                break;
            case EAST: head.move(1,0);
                break;
            case SOUTH: head.move(0,-1);
                break;
            case WEST: head.move(-1, 0);
                break;
            default: System.out.println("SNAKE DIR IS NULL");break;            
        }

        if(queued != null) {
            current = queued;
            queued = null;
        }
    }

    /**
     * Set the direction the head of the snake should move
     * @param direction the direction to move
     */
    public void setDirection(Direction direction) {
        Direction test = (current == null) ? head.getDirection() : current;
        switch(test) {
            case EAST:
            case WEST:
                if(direction == Direction.NORTH
                        || direction == Direction.SOUTH)
                    queueDirection(direction);
                break;
            case NORTH:
            case SOUTH:  
                if(direction == Direction.EAST
                        || direction == Direction.WEST)
                    queueDirection(direction);
                break;
        }
    }

    /**
     * @return the direction the head of the snake if moving
     */
    public Direction getDirection() {
        if(current != null)
            return current;
        else
            return head.getDirection();
    }

    public int getHeadX(){
        return head.getBounds().x;
    }

    public int getHeadY(){
        return head.getBounds().y;
    }

    /**
     * @return whether or not the snake is active in the game world.
     */
    @Override
    public boolean isActive() {
        return active;
    }

    /**
     * Re-spawns the snake with a length of 5 at the give spawn
     * @param point the spawn point to spawn at
     */
    public void respawn(SpawnPoint point) {        
        active = true;

        current = queued = null;
        // Create the snakes head and tail
        head.getBounds().setLocation(point.getX(), point.getY());
        head.setDirection(point.getDirection());
        

        if(!powered) {
            if(nodeCounter > 0) {
                head.getNext().setActive(false);
            }
            
            nodeCounter = 0;
            lastNode = head;
            int num = (newLength < spawnSize) ? spawnSize:newLength;

            // Add the body to the snake
            for (int i = 0; i<num; i++){
                addSnakeNode();
            }
        }
    }

    private void queueDirection(Direction direction) {
        if(direction == null) return;
        if(current == null) {
            current = direction;
            queued = null;
        }
        else
            queued = direction;
    }

    public LinkedList<SnakeNode> getNodes() {
        LinkedList<SnakeNode> out = new LinkedList<SnakeNode>();
        SnakeNode current = head;
        while(current != null) {
            out.add(current);
            current = current.getNext();
        }
        return out;
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof MasterSnake) {
            MasterSnake other = (MasterSnake) o;
            if(other.key == this.key) return true;
            else return false;
        } else return false;
    }

    @Override
    public String getDraw() {
        return "";
    }

    public void clearQueue() {
        if(current != null)
            setDirection(current);
        current = null;
        queued = null;
    }

    public int getColor() {
        return color;
    }

    public void setInvincible(boolean i) {
        powered = i;
    }
}