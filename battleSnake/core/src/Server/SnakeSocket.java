/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import com.battleSnake.alex.MasterSnake;
import com.battleSnake.kess.RunArgs;
import java.net.Socket;
import com.battleSnake.kess.Runnable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 * @author kdunc004
 */
public class SnakeSocket implements Runnable {
    protected MasterSnake player;
    Socket socket;
    
    public SnakeSocket(MasterSnake p, Socket s) {
        player = p;
        socket = s;
    }
    
    public MasterSnake getSnake() {
        return player;
    }
    
    public boolean isReady() {
        return socket.isConnected();
    }

    @Override
    public void run(RunArgs args) {
        player.run(args);
    }

    @Override
    public boolean isActive() {
        return player.isActive();
    }
    
    public OutputStream getOutputStream() throws IOException {
        return socket.getOutputStream();
    }
    
    public InputStream getInputStream() throws IOException {
        return socket.getInputStream();
    }
    
    public void close() throws IOException {
        socket.close();
    }
}
