package Server;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.battleSnake.dan.Tile;
import com.battleSnake.dan.Wall;
import com.battleSnake.kess.Drawable;
import com.battleSnake.kess.GameTimer;
import java.io.*;
import java.net.Socket;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The client is responsible for sending user input to the server as well as
 * drawing the current game state to the screen.
 * @author kdunc004
 */
public class GameClient {
    public static Texture MASTER_SPRITE;
    
    private Socket server;
    private HashMap<Integer, DrawPacket> drawables = new HashMap<Integer, DrawPacket>();
    private CommThread read, write;
    
    private BufferedReader in;
    private PrintWriter out;
    private GameTimer timer;
    
    private Input input;
    
    private boolean frameReady = true, run = true;
    
    long startTime = System.currentTimeMillis();
    
    /**
     * @param s the socket to communicate with the server
     */
    public GameClient(Socket s, Tile[][] arena) {
        for(Tile[] row : arena) {
            for(Tile tile:row) {
                if(tile instanceof Wall) {
                    DrawPacket tmp = new DrawPacket(tile.getX(), tile.getY(), 
                            Color.BLACK, 0);
                    drawables.put(tile.key, tmp);
                }
            }
        }
        server = s;
        timer = new GameTimer(arena, 0);
        
        try {
            in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            out = new PrintWriter(s.getOutputStream(), true);
        } catch (IOException ex) {
            Logger.getLogger(GameClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        input = new Input();
        Gdx.input.setInputProcessor(input);
        
        read = new CommThread(true);
        write = new CommThread(false);
        read.start();
        write.start();
    }
    
    public void draw(SpriteBatch b) {
        synchronized (drawables) {
            timer.draw(b);
            for(Drawable d : drawables.values())
                d.draw(b);
        }
    }
    
    public boolean getRun() {
        return run;
    }
    
    public void dispose() {      
        try {
            run = false;
            out.close();
            read.join();
            write.join();
            MASTER_SPRITE.dispose();
            server.close();
        } catch (IOException ex) {
            Logger.getLogger(GameClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(GameClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
    private class CommThread extends Thread {
        private boolean reader = false;
        
        public CommThread(boolean r) {
            reader = r;
        }
        @Override
        public void run() {            
            while(run) {  
                if(!reader) {                        
                        //Write direction
                        out.flush();
                        out.println(input.input);                        
                } else {
                    try {
                        //Read Drawables
                        String line = "";
                        while(!(line = in.readLine()).equals("END")) {
                            Scanner parser = new Scanner(line);
                            parser.useDelimiter(",");
                            
                            if(parser.hasNextInt()) {
                                int count = 0;
                                int[] data = new int[4];

                                while(parser.hasNextInt()) {
                                    data[count++] = parser.nextInt();
                                }

                                if(count < 4) {System.out.println("Bad Read");}//bad read
                                else {
                                    synchronized (drawables) {
                                        if(drawables.containsKey(data[0])) {
                                            drawables.get(data[0]).setX(data[1]);
                                            drawables.get(data[0]).setY(data[2]);
                                            drawables.get(data[0]).setColor(data[3]);
                                        } else {
                                            drawables.put(data[0], 
                                                    new DrawPacket(data[1], data[2], 
                                                            Color.WHITE, 0));
                                            drawables.get(data[0]).setColor(data[3]);
                                        }
                                    }
                                }
                            } else {
                                parser.next(); //throwaway "TIMER"
                                timer.set(parser.next());
                            }
                        }   
                    } catch (IOException ex) {
                        Logger.getLogger(GameClient.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
    
    private class Input implements InputProcessor {
        public char input = 'a';
        @Override
        public boolean keyDown(int i) {
            switch(i) {
                case com.badlogic.gdx.Input.Keys.W:
                    input = 'N';
                    break;
                case com.badlogic.gdx.Input.Keys.A:
                    input = 'W';
                    break;
                case com.badlogic.gdx.Input.Keys.S: 
                    input = 'S';
                    break;
                case com.badlogic.gdx.Input.Keys.D:
                    input = 'E';
                    break;
                case com.badlogic.gdx.Input.Keys.SPACE:
                    input = 'B';
                    break; 
            }
            return true;
        }

        @Override
        public boolean keyUp(int i) {
            return false;        
        }

        @Override
        public boolean keyTyped(char c) {
            return false;        
        }

        @Override
        public boolean touchDown(int i, int i1, int i2, int i3) {
            return false;        
        }

        @Override
        public boolean touchUp(int i, int i1, int i2, int i3) {
            return false;        
        }

        @Override
        public boolean touchDragged(int i, int i1, int i2) {
            return false;        
        }

        @Override
        public boolean mouseMoved(int i, int i1) {
            return false;        
        }

        @Override
        public boolean scrolled(int i) {
            return false;
        }
        
    }
}
