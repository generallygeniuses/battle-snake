package Server;

import com.badlogic.gdx.graphics.Color;

/**
 *
 * @author kdunc004
 */
public class ColorParser {
    public static Color getColor(int color) {
        switch(color) {
            case 0: return Color.GREEN; // snake
            case 1: return Color.RED; // food
            case 2: return Color.BLACK; // wall
            default: return Color.BLUE; // background
        }
    }
    
    public static int storeColor(Color c) {
        String tmp = c.toString();
        if(tmp.equalsIgnoreCase("GREEN"))
            return 0;
        else if(tmp.equalsIgnoreCase("RED"))
            return 1;
        else if(tmp.equalsIgnoreCase("BLACK"))
            return 2;
        else return -1;
    }
}
