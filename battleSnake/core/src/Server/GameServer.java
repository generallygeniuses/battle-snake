package Server;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.battleSnake.alex.MasterSnake;
import com.battleSnake.alex.SnakeNode;
import com.battleSnake.alex.SpawnPoint;
import com.battleSnake.dan.Tile;
import com.battleSnake.dan.Wall;
import com.battleSnake.kess.*;
import com.battleSnake.kess.Power.SpeedBoost;
import com.battleSnake.kess.Runnable;
import com.battleSnake.kess.ai.AI;
import com.battleSnake.kess.ai.PathAI;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author kdunc004
 */
public class GameServer {    
    private LinkedList<Collidable> colliders = new LinkedList<Collidable>();
    private final LinkedList<Runnable> runners = new LinkedList<Runnable>();
    private LinkedList<Drawable> drawables = new LinkedList<Drawable>();
    private Tile[][] arena;
    
    private SnakeSocket[] players;
    private ServerSocket server;
    private MasterSnake host;
    private AI[] cpus;
    private FoodSpawner food;
    private RunThread runThread = null;
    private LinkedList<Thread> comms = new LinkedList<Thread>();
    private GameTimer timer;
    
    private static final Random rand = new Random(System.currentTimeMillis());
    private boolean run = true, drawReady = true;
    
    /**
     * @param connections the players connected to the server
     * @param ai the number of ai in the game
     * @param map the arena the game is played in.
     */
    public GameServer(Socket[] connections, int ai, Tile[][] map) {
        try {
            int pCount = 0;
            arena = map;
            players = new SnakeSocket[connections.length];
            if(connections.length > 0)
                server = new ServerSocket(9999);
                        
            //Load map into lists
            for(Tile[] row: map) {
                for(Tile tile : row) {
                    if(tile instanceof Wall) {
                        colliders.add((Wall)tile);
                        drawables.add(tile);
                    } else if(tile instanceof SpawnPoint) {
                        colliders.add((SpawnPoint) tile);
                        runners.add((SpawnPoint)tile);
                    }
                }
            }
            
            timer = new GameTimer(map, 0);
            drawables.add(timer);
            runners.add(timer);
            
            //Spawn the food
            food = new FoodSpawner(0);
            runners.add(food);
            drawables.add(food);
            
            //Construct Snakes.
            host = new MasterSnake(getSpawn(), pCount++, 1);
            Gdx.input.setInputProcessor(new SnakeController(host, this));
            runners.add(host);
            drawables.add(host);
            
            cpus = new AI[ai];
            for(int i = 0; i < cpus.length; i++) {
                cpus[i] = new PathAI(new MasterSnake(getSpawn(), pCount++, 1));
                runners.add(cpus[i]);
                drawables.add(cpus[i].getSnake());
            }
            
            //Spawn other players
            for(int i = 0; i < connections.length; i++) {
                players[i] = new SnakeSocket(new MasterSnake(getSpawn(), pCount++, 1),
                        server.accept());
                
                runners.add(players[i]);
                drawables.add(players[i].getSnake());
                
                //make comms threads
                comms.add(new ReadThread(players[i]));
                comms.getLast().start();
                
                comms.add(new WriteThread(players[i]));
                comms.getLast().start();
            }
        } catch (IOException ex) {
            Logger.getLogger(GameServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void draw(SpriteBatch b) {
        synchronized(drawables) {
            for(Drawable d: drawables)
                d.draw(b);
        }
        
        if(runThread == null) {
            runThread = new RunThread();
            runThread.start();
        }
    }
    
    public void dispose() {
        //run is false
        run = false;
        try {
            for(Thread c : comms) {
                System.out.println("JOINING COMMS");
                c.join();
            }
            System.out.println("JOINING RUN");
            runThread.join();
            System.out.println("DONE");
        } catch (InterruptedException ex) {
            Logger.getLogger(GameServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void addBoost(SpeedBoost run) {
        synchronized(runners) {
                if(runners.contains(run)) {
                    return;
                } //do nothing   
            runners.add(run);
        }
    }
    
    public boolean getRun() {
        return run;
    }
    
    /**
     * Searches the spawn points at the edge of the map for an available spawn.
     * If it cannot find one it returns the tile at 1, 1.
     * @return the next available spawn point
     */
    private  SpawnPoint getSpawn() {
        //Random number from 0 - 3 to select a wall.
        //0 i = 0, 1 i = arena.length - 2, 2 j = 0, 3 j = arena[0].length - 2.
        Tile tmp;
        switch(rand.nextInt(4)) {
            case 0:
                tmp = arena[1][rand.nextInt(arena[0].length - 1)];
                if(tmp instanceof SpawnPoint) {
                    if(((SpawnPoint)tmp).getAvailability()) {
                        return (SpawnPoint)tmp;
                    }
                }
                return getSpawn();
            case 1:
                tmp = arena[arena.length - 2][rand.nextInt(arena[0].length - 1)];
                if(tmp instanceof SpawnPoint) {
                    if(((SpawnPoint)tmp).getAvailability()) {
                        return (SpawnPoint)tmp;
                    }
                }
                return getSpawn();
            case 2:
                tmp = arena[rand.nextInt(arena.length - 1)][1];
                if(tmp instanceof SpawnPoint) {
                    if(((SpawnPoint)tmp).getAvailability()) {
                        return (SpawnPoint)tmp;
                    }
                }
                return getSpawn();
            case 3: 
            default:
                tmp = arena[rand.nextInt(arena.length - 1)][arena[0].length - 2];
                if(tmp instanceof SpawnPoint) {
                    if(((SpawnPoint)tmp).getAvailability()) {
                        return (SpawnPoint)tmp;
                    }
                }
                return getSpawn();
        }
    }
    
    /**
     * Creates a list of all snakes in the game.
     * @return snakes in game
     */
    private MasterSnake[] makeSnakes() {
        MasterSnake[] tmp = new MasterSnake[players.length + 1 + cpus.length];
        int count = 0;
        for(int i = -1; i < players.length; i++) {
            if(i == -1) tmp[count] = host;
            else tmp[count] = players[i].getSnake();
            count++;
        }
        for(int i = 0; i < cpus.length; i++) {
            tmp[count++] = cpus[i].getSnake();
        }
        return tmp;
    }
    
    private void endGame() {
        //end the game
        dispose();
    }
    
    private class RunThread extends Thread {
        private long deltaTime = 0, lastTime = System.currentTimeMillis();
        private long elapsedTime = 0;
        private final int rate = 40;
        
        //Wait is a gate that stops snakes from moving 
        //multiple times in 1 millisecond
        
        @Override
        public void run() {
            while(run) {
                deltaTime = System.currentTimeMillis() - lastTime;
                lastTime = System.currentTimeMillis();
                elapsedTime += deltaTime;
                LinkedList<Object> remove = new LinkedList<Object>();

                if(elapsedTime >= rate) {                      
                    // test and handle collision
                    handleCollision();
                    
                    synchronized(runners) {
                        for(int i = 0; i < runners.size(); i++) {
                            Runnable r = runners.get(i);
                            if(!r.isActive()) {     
                                    remove.add(r);
                            } else {
                                if(r instanceof SpeedBoost)
                                    handleCollision();
                                r.run(RunArgs.makeArgs(r, arena, food.getOnBoard(), makeSnakes(), elapsedTime));
                            }
                        }      
                    }
                    drawReady = true;
                    elapsedTime = 0;
                } 
                    
                for(Object r : remove) {
                    if(r instanceof SnakeSocket) {
                        MasterSnake m = ((SnakeSocket)r).getSnake();
                        m.respawn(getSpawn());
                    } else if(r instanceof AI) {
                        MasterSnake m = ((AI)r).getSnake();
                        m.respawn(getSpawn());
                    } else if(r instanceof MasterSnake) {
                        MasterSnake m = (MasterSnake) r;
                        m.respawn(getSpawn());
                    }  else {
                        if(r instanceof Runnable) {
                            if(r instanceof GameTimer) {
                                endGame();
                            }
                            synchronized(runners) {
                                runners.remove(r);
                            }
                        }
                        if(r instanceof Collidable) colliders.remove(r);
                        if(r instanceof Drawable) drawables.remove(r);
                    }
                }
            }
        }
        
        private void handleCollision() {
            MasterSnake[] snakes = makeSnakes();
            for(int i = 0; i < snakes.length; i++) {
                for(int j = i; j < snakes.length; j++) {
                    if(snakes[i] != null && snakes[j] != null) {
                        snakes[i].collides(snakes[j]);
                        if(i != j)
                            snakes[j].collides(snakes[i]);
                    }
                }
                for(Collidable c : colliders) {
                    snakes[i].collides(c);
                }
                for(Collidable c : food.getCollidables()) {
                    snakes[i].collides(c);
                }
            } 
        }
    }
    
    private class ReadThread extends Thread {
        BufferedReader in;
        SnakeSocket socket;
        
        public ReadThread(SnakeSocket s) {
            socket = s;
            try {
                in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            } catch (IOException ex) {
                Logger.getLogger(GameServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        @Override
        public void run() {
            while(run)
                try {
                    String s = in.readLine();
                    if(s!= null) {
                        switch(s.charAt(0)) {
                            case 'N': socket.player.setDirection(SnakeNode.Direction.NORTH); break;
                            case 'E': socket.player.setDirection(SnakeNode.Direction.EAST); break;
                            case 'S': socket.player.setDirection(SnakeNode.Direction.SOUTH); break;
                            case 'W': socket.player.setDirection(SnakeNode.Direction.WEST); break;
                            case 'B':
                                addBoost(new SpeedBoost(socket.player)); break;
                            default: break;
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(GameServer.class.getName()).log(Level.SEVERE, null, ex);
                }
            close();
        }
        
        public void close() {
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(GameServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private class WriteThread extends Thread {
        PrintWriter out;
        SnakeSocket socket;
        long startTime = System.currentTimeMillis();
        int rate = 20;
        
        public WriteThread(SnakeSocket s) {
            socket = s;
            try {
                out = new PrintWriter(s.getOutputStream(), true);
            } catch (IOException ex) {
                Logger.getLogger(GameServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        @Override
        public void run() {
            while(run) {
                while(!drawReady) 
                {
                    out.flush();
                }
                
                for(MasterSnake s: makeSnakes()) {
                    int color = s.getColor();
                    for(SnakeNode b: s.getNodes()) {
                        String draw = b.getDraw();
                        if(!b.isActive())
                            draw += "," + (-1);
                        else if(b.isHard())
                            draw += "," + color;
                        else 
                            draw += "," + (color + (MasterSnake.COLORS.length/2));
                            
                        out.println(draw);
                    }
                }

                for(Drawable b: food.getDrawables()) {
                    String draw = b.getDraw();
                    out.println(draw);
                }
                
                out.println(timer.getDraw());

                out.println("END");
                drawReady = false;
            }
            close();
        }
        
        public void close() {
            out.close();
        }
    }
}
