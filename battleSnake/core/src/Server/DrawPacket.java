package Server;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.battleSnake.alex.MasterSnake;
import com.battleSnake.dan.Tile;
import com.battleSnake.kess.Drawable;

/**
 *
 * @author kdunc004
 */
public class DrawPacket extends Drawable {
    Color color;
    int x, y;
    
    public DrawPacket(int xVal, int yVal, Color c, int depth) {
        super(depth);
        color = c;
        x = xVal;
        y = yVal;
    }
    
    public void setX(int xVal) {
        x = xVal;
    }
    
    public void setY(int yVal) {
        y = yVal;
    }
    
    public void setColor(int c) {
        if(c > -1 && c < MasterSnake.COLORS.length)
            color = MasterSnake.COLORS[c];
        else if(c == -1) 
            color = Color.CLEAR;
        else if(c == -2)
            color = Color.GOLD;
        else if(c > MasterSnake.COLORS.length)
            color = MasterSnake.COLORS[c%(MasterSnake.COLORS.length/2)];
    }

    @Override
    public void draw(SpriteBatch batch) {
        batch.setColor(color);
        batch.draw(GameClient.MASTER_SPRITE, x, y, Tile.SIZE, Tile.SIZE);
    }

    @Override
    public String getDraw() {
        return "NOT USED";
    }
}
