package com.battleSnake.desktop.menu;

import javax.swing.JPanel;

/**
 *
 * @author kdunc004
 */
public abstract class GameMenu extends JPanel {
    protected GameFrame owner;
    
    public GameMenu(GameFrame o) {
        owner = o;
    }
}
