package com.battleSnake.desktop;

import com.battleSnake.desktop.menu.GameFrame;


public class DesktopLauncher {
    public static void main (String[] arg) {
        GameFrame menu = new GameFrame();
        menu.setLocationByPlatform(true);
        menu.setVisible(true);
    }
}