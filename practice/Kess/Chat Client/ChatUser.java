import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ChatUser {
	Socket sock;
	BufferedReader in;
	PrintWriter out;
	
	public ChatUser(Socket s) throws IOException {
		sock = s;
		in = new BufferedReader(new InputStreamReader(s.getInputStream()));
		out = new PrintWriter(s.getOutputStream(), true);
	}
	
	public boolean hasInput() throws IOException {
		return in != null && in.ready();
	}
	
	public String getLine() throws IOException {
		return in.readLine();
	}
	
	public void writeLine(String o) {
		out.println(o);
	}
}
