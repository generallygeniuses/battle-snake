import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;

public class Client {

	private JFrame frmChat;
	private JTextField addField;
	private JTextField nameField;
	private JTextField chatField;
	private JTextField portField;
	private static JTextArea chat;
	private JButton connectBtn;
	
	private Socket socket;	
	private static ChatUser user;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Client window = new Client();
					window.frmChat.setVisible(true);
					
					Timer t = new Timer(1, new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							try {
								if(user != null && user.hasInput()) {
									chat.append("\n" + user.getLine());
								}
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}					
					});
					
					t.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Client() {
		initialize();
		chat.append("\n");
		connectBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
					connect();			
			}
			
		});
		
		chatField.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String line = nameField.getText() + ": " +chatField.getText();
				user.writeLine(line);
				
				chat.append("\n" + line);
				chatField.setText("");
			}
			
		});		
	}

	/**
	 * Connects the chat client to a server
	 */
	private void connect() {
		try {
			//socket = new Socket(addField.getText(), Integer.parseInt(portField.getText()));
			socket = new Socket(InetAddress.getLocalHost(), Integer.parseInt(portField.getText()));
			user = new ChatUser(socket);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmChat = new JFrame();
		frmChat.setTitle("Chat");
		frmChat.setBounds(100, 100, 450, 405);
		frmChat.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmChat.getContentPane().setLayout(null);
		
		addField = new JTextField();
		addField.setText("198.162.1.1");
		addField.setBounds(169, 335, 89, 20);
		frmChat.getContentPane().add(addField);
		addField.setColumns(10);
		
		connectBtn = new JButton("Connect");
		connectBtn.setBounds(335, 334, 89, 23);
		frmChat.getContentPane().add(connectBtn);
		
		JLabel lblIpAddressport = new JLabel("Server Address");
		lblIpAddressport.setBounds(169, 318, 103, 14);
		frmChat.getContentPane().add(lblIpAddressport);
		
		nameField = new JTextField();
		nameField.setText("user");
		nameField.setBounds(10, 335, 149, 20);
		frmChat.getContentPane().add(nameField);
		nameField.setColumns(10);
		
		JLabel lblDisplayName = new JLabel("Display Name");
		lblDisplayName.setBounds(10, 318, 76, 14);
		frmChat.getContentPane().add(lblDisplayName);
		
		chatField = new JTextField();
		chatField.setBounds(10, 269, 414, 20);
		frmChat.getContentPane().add(chatField);
		chatField.setColumns(10);
		
		portField = new JTextField();
		portField.setText("9999");
		portField.setBounds(269, 335, 60, 20);
		frmChat.getContentPane().add(portField);
		portField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Port");
		lblNewLabel.setBounds(269, 318, 46, 14);
		frmChat.getContentPane().add(lblNewLabel);
		
		chat = new JTextArea();
		chat.setWrapStyleWord(true);
		chat.setLineWrap(true);
		chat.setText("waiting for connection...");
		chat.setEditable(false);
		chat.setBounds(10, 11, 414, 247);
		frmChat.getContentPane().add(chat);
	}
}
