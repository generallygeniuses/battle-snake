import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;

public class ChatServer {
	
	public static void main(String[] args) {
		int port = Integer.parseInt(args[0]);
		
		try {
			ServerSocket server = new ServerSocket(port, 0, InetAddress.getLocalHost());
			Client a = new Client(server.accept());
			a.writeLine("Connected. Waiting for partner...");
			System.out.println("a is connected.");
			
			Client b = new Client(server.accept());
			a.writeLine("Ready to chat!");
			b.writeLine("Connected. Ready to chat!");
			System.out.println("b is connected.");
			
			while(true) {
				if(a.sock.isClosed()) {
					b.writeLine("Partner has left. Ending session.");
					break;
				} else if(b.sock.isClosed()) {
					a.writeLine("Partner has left. Ending session.");
					break;
				}
				
				if(a.hasInput()) {
					String s = a.getLine();
					b.writeLine(s);
					System.out.println(s);
				}
				if(b.hasInput()) {
					String s = b.getLine();
					a.writeLine(s);
					System.out.println(s);
				}
			}
			server.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
