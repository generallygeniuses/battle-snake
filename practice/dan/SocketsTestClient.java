import java.io.*;
import java.net.*;

public class SocketsTestClient {
    public static void main(String argv[]) throws Exception {
        String sentence;
        String modifiedSentence;
        BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));

        Socket Socket = new Socket("localhost", 6789);
        DataOutputStream outToServer = new DataOutputStream(Socket.getOutputStream());
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(Socket.getInputStream()));

        sentence = inFromUser.readLine();
        outToServer.writeBytes(sentence + '\n');
        modifiedSentence = inFromServer.readLine();
        System.out.println(modifiedSentence);
        Socket.close();
    }
}