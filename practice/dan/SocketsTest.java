import java.io.*;
import java.net.*;

public class SocketsTest {
    public static void main(String args[]) throws Exception {
        String sentence;
        String editedSentence;
        ServerSocket Socket = new ServerSocket(6789);

        while(true) {
            Socket connectionSocket = Socket.accept();
            BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
            DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
            sentence = inFromClient.readLine();
            editedSentence = sentence.toUpperCase() + '\n';
            outToClient.writeBytes(editedSentence);
        }
    }
}