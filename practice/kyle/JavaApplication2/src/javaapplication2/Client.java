/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 *
 * @author kwill036
 */
public class Client {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try{
            Socket firstSocket = new Socket(args[0], Integer.parseInt(args[1]));
            PrintWriter out = new PrintWriter(firstSocket.getOutputStream(), true);
            
            BufferedReader sIn = new BufferedReader(
                new InputStreamReader(firstSocket.getInputStream()));
            
            BufferedReader in = new BufferedReader(
                new InputStreamReader(System.in));
            
            out.println(in.readLine());
            System.out.println(sIn.readLine());
        }
        catch(IOException theError){
            System.out.println(theError.getMessage());
        }
    }
    
}
